/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for t`he specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const mkdirp = require('mkdirp');
var admin = require("firebase-admin");
var serviceAccount = require("./happiness-machine-9e471-firebase-adminsdk-xt2ch-060963b6bf.json");
const app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://happiness-machine-9e471.firebaseio.com"
});
const spawn = require('child-process-promise').spawn;
const sharp = require("sharp");
const path = require('path');
const os = require('os');
const fs = require('fs');

// Max height and width of the thumbnail in pixels.
const THUMB_MAX_HEIGHT = 150;
const THUMB_MAX_WIDTH = 150;
// Thumbnail prefix added to file names.
const THUMB_PREFIX = 'thumbnail_';
const BUCKET_NAME = 'happiness-machine-9e471.appspot.com';

// exports.uploadFile = functions.storage.object().onFinalize(async (object) => {
//     const filePath = object.name;
//     const contentType = object.contentType;
//     const bucket = admin.storage().bucket(object.bucket);
//     const config = {action: 'read', expires: '03-01-2500',};
//     let fileUrl = null;
//     let thumbFileUrl = null;
//     let thumbFilePath = null;
//
//     let urlParts = filePath.split('/');
//     const fileName = urlParts[urlParts.length - 1];
//     const fileId = fileName.split('.')[0];
//     const fileExtension = fileName.split('.')[1];
//     const typeName = urlParts[urlParts.length - 2];
//     const itemId = urlParts[urlParts.length - 3];
//     const collectionType = urlParts[urlParts.length - 4];
//     let fileWidth = null;
//     let fileHeight = null;
//
//     // Generate thumbnail
//     if (contentType.startsWith('image/')) {
//         if (fileName.startsWith(THUMB_PREFIX)) {
//             return console.log('Already a Thumbnail.');
//         }
//
//         const fileDir = path.dirname(filePath);
//         thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));
//         const tempLocalFile = path.join(os.tmpdir(), filePath);
//         const tempLocalDir = path.dirname(tempLocalFile);
//         const tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);
//
//         const file = bucket.file(filePath);
//         const thumbFile = bucket.file(thumbFilePath);
//         const metadata = {contentType: contentType};
//
//         // Create the temp directory where the storage file will be downloaded.
//         await mkdirp(tempLocalDir)
//         // Download file from bucket.
//         await file.download({destination: tempLocalFile});
//         await sharp(tempLocalFile)
//             .toBuffer({ resolveWithObject: true })
//             .then(({ data, info }) => {
//                 fileWidth = info.width;
//                 fileHeight = info.height;
//             })
//             .catch(err => {
//                 console.log(err);
//             });
//         await sharp(tempLocalFile).resize(THUMB_MAX_WIDTH, THUMB_MAX_HEIGHT).toFile(tempLocalThumbFile);
//         // Uploading the Thumbnail.
//         await bucket.upload(tempLocalThumbFile, {destination: thumbFilePath, metadata: metadata});
//         // Once the image has been uploaded delete the local files to free up disk space.
//         fs.unlinkSync(tempLocalFile);
//         fs.unlinkSync(tempLocalThumbFile);
//
//         // Get the Signed URLs for the thumbnail and original image.
//         const results = await Promise.all([thumbFile.getSignedUrl(config), file.getSignedUrl(config),]);
//         const thumbResult = results[0];
//         const originalResult = results[1];
//         thumbFileUrl = thumbResult[0];
//         fileUrl = originalResult[0];
//     } else {
//         // Get the Signed URLs for original file
//         const file = bucket.file(filePath);
//         const results = await Promise.all([file.getSignedUrl(config)]);
//         const originalResult = results[0];
//         fileUrl = originalResult[0];
//     }
//
//     // Add the URLs to the Database
//     let item = admin.firestore().collection(collectionType).doc(itemId);
//     let itemData = await item.get();
//     let files = itemData.data().files || [];
//
//     files.push({
//         id: fileId,
//         fileName: fileName,
//         filePath: filePath,
//         type: contentType,
//         typeName: typeName,
//         extension: fileExtension,
//         thumbFilePath: thumbFilePath,
//         src: fileUrl,
//         thumbnail: thumbFileUrl,
//         fileWidth: fileWidth,
//         fileHeight: fileHeight,
//         position: files.length
//     });
//
//     await item.update({files: files});
//
//     return console.log('File successfully uploaded to storage');
// });


// Generate image thumbnail and other sizes
exports.imageProcessing = functions.https.onCall(async (data, context) => {
    // Authentication / user information is automatically added to the request.
    // const uid = context.auth.uid;
    // const name = context.auth.token.name || null;
    // const picture = context.auth.token.picture || null;
    // const email = context.auth.token.email || null;

    const filePath = data.path;
    const bucket = admin.storage().bucket(BUCKET_NAME);
    const config = {action: 'read', expires: '03-01-2500',};

    let urlParts = filePath.split('/');
    const fileName = urlParts[urlParts.length - 1];
    const fileDir = path.dirname(filePath);
    const thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));
    const tempLocalFile = path.join(os.tmpdir(), filePath);
    const tempLocalDir = path.dirname(tempLocalFile);
    const tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);

    const file = bucket.file(filePath);
    const thumbFile = bucket.file(thumbFilePath);

    // Create the temp directory where the storage file will be downloaded.
    await mkdirp(tempLocalDir)
    // Download file from bucket.
    await file.download({destination: tempLocalFile});
    await sharp(tempLocalFile).resize(THUMB_MAX_WIDTH, THUMB_MAX_HEIGHT).toFile(tempLocalThumbFile);
    // Uploading the Thumbnail.
    await bucket.upload(tempLocalThumbFile, {destination: thumbFilePath});
    // Once the image has been uploaded delete the local files to free up disk space.
    fs.unlinkSync(tempLocalFile);
    fs.unlinkSync(tempLocalThumbFile);

    // Get the Signed URLs for the thumbnail and original image.
    const results = await Promise.all([thumbFile.getSignedUrl(config)]);
    const thumbResult = results[0];
    const thumbFileUrl = thumbResult[0];

    return {thumbnailUrl: thumbFileUrl};
});

exports.deleteDream = functions.firestore
    .document('dreams/{dreamID}')
    .onDelete((snap, context) => {
        const {dreamID} = context.params;
        const bucket = admin.storage().bucket(BUCKET_NAME);
        return bucket.deleteFiles({prefix: `dreams/${dreamID}/`});
    });

exports.deleteSwitch = functions.firestore
    .document('switches/{switchID}')
    .onDelete((snap, context) => {
        const {switchID} = context.params;
        const bucket = admin.storage().bucket(BUCKET_NAME);
        return bucket.deleteFiles({prefix: `switches/${switchID}/`});
    });

exports.deleteThanks = functions.firestore
    .document('thanks/{thanksID}')
    .onDelete((snap, context) => {
        const {thanksID} = context.params;
        const bucket = admin.storage().bucket(BUCKET_NAME);
        return bucket.deleteFiles({
            prefix: `thanks/${thanksID}/`
        });
    });

exports.deleteFile = functions.storage.object().onDelete(async (object) => {
    const filePath = object.name;
    const contentType = object.contentType;
    const fileDir = path.dirname(filePath);
    const fileName = path.basename(filePath);
    const thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));

    // Exit if this is triggered on a file that is not an image.
    if (!contentType.startsWith('image/')) {
        return console.log('This is not an image.');
    }

    // Exit if the image is already a thumbnail.
    if (fileName.startsWith(THUMB_PREFIX)) {
        return console.log('This is a Thumbnail.');
    }

    const bucket = admin.storage().bucket(object.bucket);
    const thumbFile = bucket.file(thumbFilePath);
    await thumbFile.delete();

    return console.log('Thumbnail successfully deleted.');
});