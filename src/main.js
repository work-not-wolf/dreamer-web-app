import App from "./App";
import Vue from 'vue'
import Vuetify from 'vuetify/lib';
import store from './modules/store/store';
import router from './modules/router/router';
import VueGridLayout from 'vue-grid-layout';
import VueMq from 'vue-mq';
import './modules/firebase/firebase';
import VuePlyr from 'vue-plyr';
import './registerServiceWorker'

Vue.use(Vuetify);
Vue.use(VueGridLayout);
Vue.use(VueMq, {
  breakpoints: {
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity,
  }
});
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false }
  },
  emit: ['ended']
})

Vue.config.productionTip = false

let app = new Vue({
  vuetify: new Vuetify({}),
  store,
  router,
  render: h => h(App)
}).$mount('#app')

export {app};