/**
 * Prepare array for VueGridLayout component:
 * Set x, y and w, h property
 * @param [array]: List of objects
 * @param [cols]: Size of columns
 * @return Array
 */
function prepareArray(array, cols) {
    let newArray = [];
    let defaultWidth = 2;
    let defaultHeight = 4;
    let startX = 0;
    let startY = 0;

    array.sort((a, b) => parseInt(a.i) - parseInt(b.i)).map((item, index) => {
        let obj = Object.assign({}, item);

        obj.x = (item.x !== undefined) ? item.x : startX;
        obj.y = (item.y !== undefined) ? item.y : startY;
        obj.w = (item.w !== undefined) ? item.w : defaultWidth;
        obj.h = (item.h !== undefined) ? item.h : defaultHeight;
        obj.i = index.toString();

        startX += obj.w;
        if (startX >= cols) {
            startX = 0;
            startY += obj.h;
        }

        newArray.push(obj);
    });

    return newArray;
}

export {prepareArray};
