import {_saveUserSettings, _getItems, _getItem} from '../firebase/db';

export default {
    saveSettings({commit}, {settings}) {
        _saveUserSettings(settings)
            .then(() => {
                commit('setUserSettings', {settings: settings});
            })
            .catch((error) => {
                commit('showSnackbar', {text: error.message, color: 'error'});
            });
    },

    getItems({commit, state}) {
        let items = []
        commit('setLoading', {key: `list.load`, value: true});

        _getItems(state.activeCollectionType, state.currentUser.uid)
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let data = doc.data();
                    let images = data.files ? data.files.filter((item) => item.typeName === 'image') : [];
                    data.src = images.length ? images[0].src : state.defaultItemImage;
                    items.push(data);
                });

                commit('saveItems', items);

                commit('setLoading', {key: `list.load`, value: false});
            })
            .catch((error) => {
                commit('showSnackbar', {text: error.message, color: 'error'});
            });
    },

    getItem({commit, state}, {itemId}) {
        commit('setLoading', {key: 'item.load', value: true});

        _getItem(state.activeCollectionType, itemId)
            .then((doc) => {
                if (doc.exists) {
                    let item = doc.data();
                    commit('setCurrentItem', {item: item});
                    commit('setLoading', {key: 'item.load', value: false});
                } else {
                    commit('showSnackbar', {text: 'Item not found', color: 'error'});
                }
            })
            .catch((error) => {
                commit('showSnackbar', {text: error.message, color: 'error'});
            });
    }
}