import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import VuexPersist from 'vuex-persist';

const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: window.localStorage
})

Vue.use(Vuex);

let store = {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions,
    plugins: [vuexPersist.plugin]
}

export default new Vuex.Store(store);