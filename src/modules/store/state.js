export default {
    currentUser: null,
    userSettings: null,
    defaultUserSettings: {
        theme: 'light'
    },
    activeCollectionType: 'dreams',
    defaultCollections: {
        dreams: [],
        switches: [],
        thanks: []
    },
    collections: {},
    currentItem: null,
    settings: {
        loading: {
            dreams: {
                settings: {
                    load: false,
                    save: false
                },
                list: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false
                },
                item: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false,
                    files: {
                        load: false,
                        delete: false
                    }
                }
            },
            switches: {
                settings: {
                    load: false,
                    save: false
                },
                list: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false
                },
                item: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false,
                    files: {
                        load: false,
                        delete: false
                    }
                }
            },
            thanks: {
                settings: {
                    load: false,
                    save: false
                },
                list: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false
                },
                item: {
                    load: false,
                    save: false,
                    update: false,
                    delete: false,
                    files: {
                        load: false,
                        delete: false
                    }
                }
            },
        },
        editing: {
            dreams: {
                list: false,
                item: false
            },
            switches: {
                list: false,
                item: false
            },
            thanks: {
                list: false,
                item: false
            }
        },
        gridParams: {
            cols: {
                mobile: 12,
                tablet: 12,
                laptop: 12,
                desktop: 12
            },
            rows: {
                mobile: 15,
                tablet: 20,
                laptop: 30,
                desktop: 30
            },
            margin: {
                mobile: [5, 5],
                tablet: [5, 5],
                laptop: [5, 5],
                desktop: [5, 5]
            }
        },
    },
    themes: [
        {
            name: 'light',
            description: 'White background and black text',
            colors: {
                main: '#FFFFFF',
                text: 'black--text',
                link: 'rgb(150, 54, 148)',
                progressbar: 'rgb(150, 54, 148)',
                linkText: 'purple--text',
                button: '#FFFFFF',
                buttonText: 'black--text',
                buttonSubText: 'grey--text',
                placeholder: ''
            },
            button: {
                fab: true,
                dark: false,
                textOrIcon: true
            },
            dark: false
        },
        {
            name: 'dark',
            description: 'Black background and white text',
            colors: {
                main: 'rgb(36, 37, 42)',
                text: 'white--text',
                link: 'white',
                progressbar: 'white',
                button: 'rgb(80,80,90)',
                buttonText: 'white--text',
                buttonSubText: 'white--text',
                placeholder: 'dark-theme'
            },
            button: {
                fab: true,
                dark: true,
                textOrIcon: false
            },
            dark: true
        }
    ],

    snackbar: {
        show: false,
        text: '',
        color: 'info'
    },

    defaultItemImage: 'https://firebasestorage.googleapis.com/v0/b/happiness-machine-9e471.appspot.com/o/default%2Fjohannes-plenio-DKix6Un55mw-unsplash.jpg?alt=media&token=212a39bd-4f66-4ad8-80d3-8afe8bc5da79',

    carousel: {
        active: false,
        index: 0,
        cycle: false,
        interval: 0
    }
}