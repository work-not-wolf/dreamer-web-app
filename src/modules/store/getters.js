import {app} from './../../main';

export default {
    displayAsIcon: function() {
        return app.$mq === 'mobile' || app.$mq === 'tablet';
    },
    theme: function(state) {
        if (state.userSettings && state.userSettings.theme) {
            let _themes = state.themes.filter(item => item.name === state.userSettings.theme);

            if (_themes.length) {
                return _themes[0];
            }
        }
        return state.themes.filter(item => item.name === state.defaultUserSettings.theme)[0];
    },
    items: function(state) {
        return state.defaultCollections[state.activeCollectionType];
    },
    itemListLoadMode: function(state) {
        return state.settings.loading[state.activeCollectionType].list.load;
    },
    itemLoadMode: function(state) {
        return state.settings.loading[state.activeCollectionType].item.load;
    },
    itemListEditMode: function(state) {
        return state.settings.editing[state.activeCollectionType].list;
    },
    itemEditMode: function(state) {
        return state.settings.editing[state.activeCollectionType].item;
    }
};