export default {
    setActiveCollectionType(state, {collectionType}) {
        state.activeCollectionType = collectionType;
    },

    setUserSettings(state, {settings}) {
        state.userSettings = settings;
    },

    saveItems(state, items) {
        state.defaultCollections[state.activeCollectionType] = items;
    },

    setCurrentItem(state, {item}) {
        state.currentItem = item;
    },

    setUser(state, {user}) {
        state.currentUser = user;
    },

    showSnackbar(state, {text, color}) {
        state.snackbar.text = text;
        state.snackbar.color = color;
        state.snackbar.show = true;
    },
    closeSnackbar(state) {
        state.snackbar.text = '';
        state.snackbar.color = 'info';
        state.snackbar.show = false;
    },

    setLoading(state, {key, value}) {
        let settingsObject = state.settings.loading[state.activeCollectionType];
        let parts = key.split('.');
        for (let i=0; i < parts.length - 1; i++) {
            let _key = parts[i];
            settingsObject = settingsObject[_key];
        }
        settingsObject[parts[parts.length - 1]] = value;
    },

    setEditing(state, {key, value}) {
        let settingsObject = state.settings.editing[state.activeCollectionType];
        let parts = key.split('.');
        for (let i=0; i < parts.length - 1; i++) {
            let _key = parts[i];
            settingsObject = settingsObject[_key];
        }
        settingsObject[parts[parts.length - 1]] = value;
    },

    showCarousel(state, {index}) {
        state.carousel = {
            active: true,
            index: index || 0
        };
    },

    setCarouselSlide(state, {index}) {
        state.carousel.index = index;
    },

    prevCarouselSlide(state) {
        state.carousel.index = state.carousel.index - 1;
    },

    nextCarouselSlide(state) {
        state.carousel.index = state.carousel.index + 1;
    },

    closeCarousel(state) {
        state.carousel = {
            active: false,
            index: 0
        };
    }
}