import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';
import store from '../store/store';
import Login from '../../components/Auth/Login';
import Register from '../../components/Auth/Register';
import ItemList from "../../components/Collections/ItemList";
import Item from "../../components/Collections/Item";

Vue.use(Router);

let router = new Router({
    routes: [
        {
            name: 'login',
            path: '/',
            component: Login,
            meta: {
                requiresAuth: false
            }
        },
        {
            name: 'register',
            path: '/register',
            component: Register,
            meta: {
                requiresAuth: false
            }
        },
        {
            name: 'collections',
            path: '/:collectionType',
            component: ItemList,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'add-item',
            path: '/:collectionType/add',
            component: Item,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'about-item',
            path: '/:collectionType/about/:itemId',
            component: Item,
            props: true,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

// Nav Guard
router.beforeEach((to, from, next) => {
    // Check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // Load user
        firebase.auth().onAuthStateChanged(user => {
            // If user obj does not exist --> redirect to login page
            if (!user) {
                next({ name: 'login' });
            } else {
                store.commit("setUser", {user: user});
                next();
            }
        });
    } else {
        // Proceed to route
        next();
    }
});

export default router;
