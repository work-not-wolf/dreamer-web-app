import {db} from './firebase';

const dbUserSettings = db.collection('user-settings');
const dreamsCollection = db.collection('dreams');
const switchesCollection = db.collection('switches');
const thanksCollection = db.collection('thanks');

const dbCollections = {
    dreams: dreamsCollection,
    switches: switchesCollection,
    thanks: thanksCollection
};

function _getUserSettings(userId) {
    return dbUserSettings.where("user_id", "==", userId).get();
}

function _saveUserSettings(settings) {
    return dbUserSettings.doc(settings.id).update(settings);
}

function _getItems(collectionType, userId) {
    return dbCollections[collectionType]
        .where("user_id", "==", userId)
        .where("status", "==", 'active')
        .get();
}

function _getItem(collectionType, id) {
    return dbCollections[collectionType].doc(id).get();
}

function _deleteItems(collectionType, ids) {
    ids.forEach(async (id) => {
        await _deleteItem(collectionType, id);
    })
}

function _deleteItem(collectionType, id) {
    return dbCollections[collectionType].doc(id).delete();
}

export {dbCollections, dbUserSettings, _getUserSettings, _saveUserSettings, _getItems, _getItem, _deleteItems, _deleteItem};