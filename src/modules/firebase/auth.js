import firebase from 'firebase';

function _login(email, password) {
    return firebase
        .auth()
        .setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(() => {
            return firebase.auth().signInWithEmailAndPassword(email, password);
        })
}

function _register(email, password) {
    return firebase
        .auth()
        .createUserWithEmailAndPassword(
            email,
            password
        )
}

function _logout() {
    return firebase
        .auth()
        .signOut();
}

function _getCurrentUser() {
    return firebase
        .auth()
        .currentUser;
}

function _resetPassword(email) {
    return firebase
        .auth()
        .sendPasswordResetEmail(
            email
        )
}

export {_login, _register, _logout, _getCurrentUser, _resetPassword};
