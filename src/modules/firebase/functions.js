import {functions} from './firebase';

let imageProcessing = functions.httpsCallable('imageProcessing');

export {imageProcessing};