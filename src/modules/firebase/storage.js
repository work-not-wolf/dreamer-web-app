import {storage} from './firebase';
// import {imageProcessing} from './functions';
let uuid = require('uuid');

const IMAGE_TYPE_NAME = 'image';
const VIDEO_TYPE_NAME = 'video';
const AUDIO_TYPE_NAME = 'audio';
const AVAILABLE_TYPES = [IMAGE_TYPE_NAME, VIDEO_TYPE_NAME, AUDIO_TYPE_NAME];
const MAX_SIZE = 30 * 1024 * 1024;


async function uploadFile(file, collectionType, itemId) {
    try {
        const {name, size, type} = file;
        const extension = name.split('.')[name.split('.').length - 1];
        const typeName = type.split('/')[0];  // image/video/audio

        if (AVAILABLE_TYPES.indexOf(typeName) === -1) {
            return {error: 'Invalid type of file'};
        }

        if (size > MAX_SIZE) {
            return {error: `File size exceeds the limit (${MAX_SIZE / 1024 / 1024} Mbytes)`};
        }

        const fileId = uuid.v1();
        const fileName = `${fileId}.${extension}`;
        const filePath = `${collectionType}/${itemId}/${typeName}/${fileName}`;
        const metaData = {
            contentType: type
        };

        const fileRef = storage.child(filePath);
        await fileRef.put(file, metaData);
        const fileUrl = await fileRef.getDownloadURL();

        return  {
            id: fileId,
            src: fileUrl,
            name: fileName,
            path: filePath,
            type: type,
            typeName: typeName
        };
    } catch (e) {
        return {error: e};
    }
}

async function deleteFile(filePath) {
    try {
        const fileRef = storage.child(filePath);
        return await fileRef.delete();
    } catch (e) {
        return {error: e};
    }
}

export {uploadFile, deleteFile};
