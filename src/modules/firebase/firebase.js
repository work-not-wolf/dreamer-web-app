const firebase = require("firebase");
require("firebase/firestore");
import firebaseConfig from './firebase_config';

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const storage = firebase.storage().ref();
const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;
const functions = firebase.functions();

export {db, storage, serverTimestamp, functions};
